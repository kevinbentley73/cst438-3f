package cst4383f.cst4383f;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cst4383fApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cst4383fApplication.class, args);
	}

}
